#include "officeworker.h"

#include <sstream>

OfficeWorker::OfficeWorker(const string &name, float salary, int officeTime)
    : Employee(name, salary), m_officeTime(officeTime)
{ }

string OfficeWorker::toString()
{
    stringstream ss;

    ss << Employee::toString();
    ss << "Office time: " << getOfficeTime() << endl;;

    return ss.str();
}

int OfficeWorker::getOfficeTime() const
{
	return m_officeTime;
}

void OfficeWorker::setOfficeTime(int officeTime)
{
	m_officeTime = officeTime;
}




