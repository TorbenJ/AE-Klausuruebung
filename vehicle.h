#ifndef VEHICLE_H
#define VEHICLE_H

#include <string>

using namespace std;

class Vehicle
{
public:
    Vehicle(const string &name);
    virtual ~Vehicle();

private:
    string m_name;
};

#endif // VEHICLE_H
