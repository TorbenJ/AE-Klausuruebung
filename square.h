#ifndef SQUARE_H
#define SQUARE_H

#include "abstractgeometricbody.h"

/**
 * Vererbung als C++-Code – Klassenrahmen ohne Inhalt in der Header.
 */

class Square : public AbstractGeometricBody
{
public:
    Square() { }

    double baseArea() override
	{
		return 0.0; // Richtigen Wert hier berechnen.
	}
};

#endif // SQUARE_H
