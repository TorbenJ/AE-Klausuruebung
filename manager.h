#ifndef MANAGER_H
#define MANAGER_H

#include "employee.h"

using namespace std;

class Manager : public Employee
{
public:
    Manager(const string &name, float salary, double bonus);

    string toString() override;

	double getBonus() const;
	void setBonus(double bonus);

private:
    double m_bonus;
};

#endif // MANAGER_H
