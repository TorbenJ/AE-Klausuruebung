#ifndef CAR_H
#define CAR_H

#include "vehicle.h"

class Part;
class Wheel;

class Car : public Vehicle
{

public:
    Car(const string& name, const string &partName, double radius, double width);
    virtual ~Car();

    double wheelSize();

private:
	Wheel* m_wheel = nullptr;
};

#endif // CAR_H
