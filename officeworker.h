#ifndef OFFICEWORKER_H
#define OFFICEWORKER_H

#include "employee.h"

using namespace std;

class OfficeWorker : public Employee
{
public:
    OfficeWorker(const string& name, float salary, int officeTime);

    string toString() override;

	int getOfficeTime() const;
	void setOfficeTime(int officeTime);

private:
    int m_officeTime;
};

#endif // OFFICEWORKER_H
