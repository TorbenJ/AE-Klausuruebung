#include "employee.h"

#include <sstream>

Employee::Employee(const string &name, float salary)
    : m_name(name), m_salary(salary)
{ }

Employee::~Employee()
{
}

string Employee::toString()
{
    stringstream ss;

    ss << "Employee Name: " << getName() << endl;;
    ss << "Salary: " << getSalary() << endl;

    return ss.str();
}

string Employee::getName() const
{
	return m_name;
}

void Employee::setName(const string& name)
{
	m_name = name;
}

float Employee::getSalary() const
{
	return m_salary;
}

void Employee::setSalary(float salary)
{
	m_salary = salary;
}








