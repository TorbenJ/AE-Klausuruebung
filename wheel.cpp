#include "wheel.h"

Wheel::Wheel(const string &name, double radius, double width)
    : Part(name), m_radius(radius), m_width(width)
{
}

double Wheel::getRadius() const
{
	return m_radius;
}

void Wheel::setRadius(double radius)
{
	m_radius = radius;
}

double Wheel::getWidth() const
{
	return m_width;
}

void Wheel::setWidth(double width)
{
	m_width = width;
}
