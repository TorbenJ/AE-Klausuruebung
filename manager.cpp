#include "manager.h"

#include <sstream>

/**
 * Konstruktor von erbenden Klassen, wobei keine default Konstruktoren existieren:
 * Bsp. Bus muss Konstruktor von Personentransportfahrzeug nutzen.
 *
 * Manager nutzt Konstruktor von Employee
 */

Manager::Manager(const string& name, float salary, double bonus)
    : Employee(name, salary), m_bonus(bonus)
{ }

string Manager::toString()
{
    stringstream ss;

    ss << Employee::toString();
    ss << "Bonus: " << getBonus() << endl;

    return ss.str();
}

double Manager::getBonus() const
{
	return m_bonus;
}

void Manager::setBonus(double bonus)
{
	m_bonus = bonus;
}