#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>

using namespace std;

class Employee
{
public:
    Employee(const string& name, float salary);
	virtual ~Employee();

    virtual string toString();

	string getName() const;
	void setName(const string &name);

	float getSalary() const;
	void setSalary(float salary);

private:
    string m_name;
    float m_salary;
};

#endif // EMPLOYEE_H
