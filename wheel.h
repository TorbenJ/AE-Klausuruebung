#ifndef WHEEL_H
#define WHEEL_H

#include "part.h"

class Wheel : public Part
{
public:
    Wheel(const string &name, double radius, double width);

	double getRadius() const;
	void setRadius(double radius);

	double getWidth() const;
	void setWidth(double width);

private:
    double m_radius;
    double m_width;
};

#endif // WHEEL_H
