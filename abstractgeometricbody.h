#ifndef ABSTRACTGEOMETRICBODY_H
#define ABSTRACTGEOMETRICBODY_H

/**
 * Klasse mit abstrakten Methoden - Header:
 * Bsp Figur2D, N_Eck, ZusammengesetzteFigur
 */

class AbstractGeometricBody
{
public:
    AbstractGeometricBody() { }
	virtual ~AbstractGeometricBody() { }

    virtual double baseArea() = 0;
};

#endif // ABSTRACTGEOMETRICBODY_H
