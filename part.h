#ifndef PART_H
#define PART_H

#include <iostream>

using namespace std;

class Part
{

public:
    Part(const string &name);

private:
    string m_name;

};

#endif // PART_H
