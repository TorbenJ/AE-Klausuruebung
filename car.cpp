#include "car.h"
#include "wheel.h"

/**
 * Konstruktor von Klasse mit Komposition
 * Bsp. Ein Stern muss ein konkretes N_Eck (z.B. ein Pentagon) und konkrete Dreiecke erstellen und
 * sie einem TeileArray – das eventuell in einer übergeordneten Klasse steht – hinzufügen.
 *  --- Wheel wird erstellt und m_parts zugewiesen
 *
 * Methode, die die Verwendung von private Attributen übergeordneter Klassen erfordert.
 * Bsp: Bus muss tankinhalt aus Kfz verwenden.
 *  --- m_parts ist aus Vehicle
 *
 * Methode, welche die Verwendung eines Kompositionsobjekts erfordert.
 * Bsp: flaeche aus Zylinder wird mit Hilfe des eingebauten Kreises berechnet.
 * Redefinitionen und Überladungen identifizieren können.
 * expliziter Aufruf einer Methode einer übergeordneten Klasse, z.B. in redefinierter Methode.
 *  --- wheelSize() greift auf m_radius von Wheel zurueck
 */

Car::Car(const string &name, const string &partName, double radius, double width)
    : Vehicle(name), m_wheel(new Wheel("Spare wheel", 5, 5))
{
}

Car::~Car()
{
	delete m_wheel;
}

double Car::wheelSize()
{
    return m_wheel->getRadius() * 2;
}

