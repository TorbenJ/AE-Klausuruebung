#include <iostream>
#include <memory>

#include "manager.h"
#include "officeworker.h"

using namespace std;

/**
 * In main ein polymorphes Objekt mit Aufruf einer redefinierten Methode.
 * Achtung: Ich will die Polymorphie sehen – heißt Container (i.d.R. Array) mit unterschiedlichen
 * Objekten durch eine Schleife laufen lassen, welche alle polymorphen Methoden aufruft.
 */

void useSmartPtr();

int main(int argc, char *argv[])
{
#define MAX_SIZE 2

    Employee** employees = new Employee*[MAX_SIZE];

    employees[0] = new Manager("Bossy McBossFace", 100000, 500.00);
    employees[1] = new OfficeWorker("Chad", 500, 40);

    for(int i = 0; i < MAX_SIZE; ++i)
        cout << employees[i]->toString() << endl;

	for (int i = 0; i < MAX_SIZE; ++i)
		delete employees[i];

    delete[] employees;

    return 0;
}

void useSmartPtr()
{
    // erstellen von shared_ptr
    shared_ptr<OfficeWorker> officeWorker
            = make_shared<OfficeWorker>("Chad", 500.f, 40);

    // erstellen von unique_ptr
    unique_ptr<Manager> manager(new Manager("Bossy McBossFace", 100000, 500.00));

    // uebergabe von unique_ptr
    shared_ptr<Manager> newManager = move(manager);

}
